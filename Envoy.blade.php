{{-- Load up the .env file --}}
@include('vendor/autoload.php');
@setup
    $dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
    $dotenv->load();
@endsetup

{{-- Add the server connection --}}
@servers(['rollog' => $_ENV['SERVER_SSH'], 'localhost' => '127.0.0.1'])

{{-- Deploy the app --}}
@task('deploy', ['on' => 'rollog'])
    cd {{ $_ENV['SERVER_DEPLOY_ROOT'] }}

    echo "UPDATING CODEBASE"
    git reset --hard origin/dev

    @if ($composer)
        echo "INSTALLING COMPOSER DEPENDENCIES"
        composer install
    @endif

    @if ($migrate)
        echo "MIGRATING DATABASE"
        php artisan migrate:fresh --seed
    @endif

    @if($npm)
        echo "INSTALLING NODE DEPENDENCIES"
        npm install

        echo "BULDING FRONTEND"
        npm run build
    @endif

    php artisan optimize:clear
@endtask

{{-- @task('scratch', ['on' => 'localhost'])
    echo {{$_ENV['ENVOY_SERVER_SSH']}}
@endtask --}}
