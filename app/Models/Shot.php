<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

const Exposures = [2, 4, 8, 15, 30, 60, 125, 250, 500, 1000, 2000];

enum ExposureKind: string
{
    case Long = 'long';
    case Standard = 'standard';
    case Custom = 'custom';
}

enum LongExposureUnit: string
{
    case Seconds = 'sec';
    case Minutes = 'min';
    case Hours = 'hrs';
}

class Shot extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $casts = [
        'flash' => 'boolean',
        'exposure' => 'array'
    ];

    protected $hidden = ['lens_id', 'roll_id'];

    protected $with = ['lens'];

    protected $fillable = ['aperture', 'exposure', 'flash', 'pushpull', 'title', 'notes'];

    /**
     * Relations.
     */
    public function lens()
    {
        return $this->belongsTo(Lens::class);
    }

    public function roll()
    {
        return $this->belongsTo(Roll::class);
    }

    /**
     * Get a shot. Enforce that it belongs to a roll.
     */
    public static function get(int $rollId, int $shotId)
    {
        return self::where('id', $shotId)
            ->where('roll_id', $rollId)
            ->firstOrFail();
    }
}
