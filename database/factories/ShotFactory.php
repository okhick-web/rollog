<?php

namespace Database\Factories;

use App\Models\Lens;
use App\Models\Shot;
use App\Models\ExposureKind;
use App\Models\LongExposureUnit;
use Illuminate\Database\Eloquent\Factories\Factory;

use const App\Models\Exposures;

class ShotFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Shot::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'exposure' => function () {
                $exposureCases = ExposureKind::cases();
                $randExposureKind = $exposureCases[array_rand($exposureCases)];

                switch ($randExposureKind) {
                    case ExposureKind::Custom:
                        return [
                            'value' => $this->faker->numberBetween(1, 1000),
                            'kind' => ExposureKind::Custom
                        ];

                    case ExposureKind::Standard:
                        return [
                            'value' => Exposures[array_rand(Exposures)],
                            'kind' => ExposureKind::Standard
                        ];

                    case ExposureKind::Long:
                        $units = LongExposureUnit::cases();

                        return [
                            'value' => $this->faker->numberBetween(1, 100),
                            'unit' => $units[array_rand($units)],
                            'kind' => ExposureKind::Long
                        ];
                }
            },
            'flash' => getRandomWeightedElement([0 => 32, 1 => 4]),
            'pushpull' => $this->faker->numberBetween(-3, 3),
            'title' => ucwords($this->faker->word()),
            'notes' => $this->faker->sentence()
        ];
    }

    /**
     * Choose lens from the user's inventory and a compatible aperture for that lens.
     *
     * @return array
     */
    public function withLensAndAperture(int $user_id)
    {
        $lens = Lens::where('user_id', $user_id)
            ->inRandomOrder()->first();

        return $this->state(function (array $attributes) use ($lens) {
            $minApertureIndex = array_search($lens->minimum_aperture, Lens::APERTURES);
            $maxApertureIndex = array_search($lens->maximum_aperture, Lens::APERTURES);

            $aperture = Lens::APERTURES[rand($minApertureIndex, $maxApertureIndex)];

            return [
                'lens_id' => $lens->id,
                'aperture' => $aperture
            ];
        });
    }
}

  /**
   * getRandomWeightedElement().
   *
   * https://gist.github.com/irazasyed/f41f8688a2b3b8f7b6df
   *
   * Utility function for getting random values with weighting.
   * Pass in an associative array, such as array('A'=>5, 'B'=>45, 'C'=>50)
   * An array like this means that "A" has a 5% chance of being selected, "B" 45%, and "C" 50%.
   * The return value is the array key, A, B, or C in this case. Note that the values assigned
   * do not have to be percentages. The values are simply relative to each other. If one value
   * weight was 2, and the other weight of 1, the value with the weight of 2 has about a 66%
   * chance of being selected. Also note that weights should be integers.
   *
   * @param array $weightedValues
   */
  function getRandomWeightedElement(array $weightedValues)
  {
      $rand = mt_rand(1, (int) array_sum($weightedValues));

      foreach ($weightedValues as $key => $value) {
          $rand -= $value;
          if ($rand <= 0) {
              return $key;
          }
      }
  }
