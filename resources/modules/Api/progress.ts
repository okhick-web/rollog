import NProgress from "nprogress";
import "nprogress/nprogress.css";

NProgress.configure({
  showSpinner: false,
  parent: "#app",
  template: `
    <div class="fixed top-0 left-0 bg-kodak h-1 w-full drop-shadow-lg" role="bar"></div>
  `,
});

export default NProgress;

// #nprogress .bar {
//   background: #29d;

//   position: fixed;
//   z-index: 1031;
//   top: 0;
//   left: 0;

//   width: 100%;
//   height: 2px;
// }
