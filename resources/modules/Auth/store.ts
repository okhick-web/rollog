import { defineStore } from "pinia";

import { Lens } from "../Core/@types";
import { User, UserState } from "./@types";

export const useAuthStore = defineStore("AuthStore", {
  state: (): UserState => {
    return {
      isLoggedIn: false,
      user: undefined,
    };
  },
  getters: {
    lenses: (state) => state.user?.lenses,

    getLens: (state) => {
      return (lensId: number) => {
        return state.user?.lenses.find((lens) => lens.id === lensId);
      };
    },
  },
  actions: {
    async fetchUser() {
      const { api, ziggy } = await import("@/modules/Api");

      try {
        const userRes = await api.get<User>(ziggy.route("user"));

        this.user = userRes.data;
        this.isLoggedIn = true;
      } catch (e) {
        // errors should be handled in the api
      }
    },

    async fetchLenses() {
      const { api, ziggy } = await import("@/modules/Api");

      try {
        const lensesRes = await api.get<Lens[]>(ziggy.route("lenses.index"));

        this.user!.lenses = lensesRes.data;
      } catch {
        // ...
      }
    },
  },
});
