export enum Button {
  Core = "h-10 py-2 px-4 font-medium drop-shadow hover:shadow-inner hover:drop-shadow-lg",
}

export enum ButtonColor {
  White = "bg-white text-black hover:bg-portra-100",
  Fuji = "bg-fuji text-white hover:bg-fuji-600",
  Portra = "bg-portra text-white hover:bg-portra-600",
  None = "bg-none text-white hover:underline",
}

export enum ButtonOutline {
  Portra = "outline outline-1 outline-t outline-b outline-r first:outline-l outline-portra hover:z-10 hover:outline-2",
}

export interface ButtonGroupButton {
  value: string | number;
  label: string;
}
