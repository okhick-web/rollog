export enum InputFieldType {
  Text = "text",
  Email = "email",
  Password = "password",
  Textarea = "textarea",
}

export interface FieldWrapperProps {
  label: string;
  right?: boolean;
}
export interface InputFieldProps {
  value: any;
  name: string;
  type: InputFieldType;
}

export interface TextareaProps {
  label: FieldWrapperProps["label"];
  value: string | undefined;
  name: string;
  rows?: number;
}

export interface TextFieldProps {
  label: FieldWrapperProps["label"];
  value?: InputFieldProps["value"];
}

export type Option = Record<string, any> & { id: number };

export interface SelectFieldProps {
  selected?: Option;
  name?: string;
  options: Option[];
  optionLabel: string | ((option: Option) => string);
  disabled?: boolean;
}

export interface SwitchFieldProps {
  label: FieldWrapperProps["label"];
  enabled: boolean;
}
