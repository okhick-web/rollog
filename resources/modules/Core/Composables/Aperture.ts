import { Lens } from "@/modules/Core/@types";

const APERTURES = (() => {
  const apertureAccum: number[] = [];

  for (let i = 0; i < 15; i++) {
    if (i < 6) {
      apertureAccum.push(Math.floor(Math.pow(Math.sqrt(2), i) * 10) / 10);
    } else {
      apertureAccum.push(Math.floor(Math.pow(Math.sqrt(2), i)));
    }
  }

  return apertureAccum;
})();

export function useAperture() {
  function getApertureSelectValues(lens: Lens) {
    // Get only the apertures needed for this lens
    const firstApertureIndex = APERTURES.indexOf(lens.minimum_aperture || 0);
    const lastApertureIndex = APERTURES.indexOf(
      lens.maximum_aperture || APERTURES.length - 1
    );

    const neededApertures = APERTURES.slice(
      firstApertureIndex,
      lastApertureIndex + 1
    );

    const apertureSliderValues = neededApertures.map((value, index) => {
      return {
        id: index,
        value: value,
        label: `ƒ${value}`,
      };
    });

    return apertureSliderValues;
  }

  return {
    getApertureSelectValues,
  };
}
