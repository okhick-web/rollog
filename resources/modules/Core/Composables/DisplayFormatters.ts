import { Camera, Lens } from "../@types";
import { ExposureKind, ExposureTime } from "./ExposureTime";

export function useDisplayFormatters() {
  function formatCamera(camera: Camera | undefined) {
    if (!camera) return "";

    return `${camera.make} ${camera.model}`;
  }

  function formatLens(lens: Lens | undefined) {
    if (!lens) return null;

    return {
      makeModel: `${lens.make} ${lens.model}`,
      aperture: `\u0192${lens.minimum_aperture}\u2013${lens.maximum_aperture}`,
    };
  }

  function formatPushPull(pushPull: number | undefined) {
    // this covers pushpull = 0
    if (!pushPull) return undefined;

    const sign = pushPull > 0;

    const stopSign = sign ? "+" : "-";
    const stopAbs = Math.abs(pushPull);
    const stopText = stopAbs > 1 ? "stops" : "stop";

    return `${stopSign}${stopAbs} ${stopText}`;
  }

  function formatExposureTime(time: ExposureTime) {
    if (time.kind === ExposureKind.Long) {
      return `${time.value} ${time.unit}`;
    }

    return formatExposureTimeHTML(time.value);
  }

  function formatExposureTimeHTML(time: number) {
    return `<sup>1</sup>&frasl;<sub>${time}</sub>`;
  }

  return {
    formatCamera,
    formatLens,
    formatPushPull,
    formatExposureTime,
    formatExposureTimeHTML,
  };
}
