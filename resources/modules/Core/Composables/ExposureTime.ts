import { useDisplayFormatters } from "@/modules/Core/Composables/DisplayFormatters";
import { Option } from "@/modules/Core/Components/Fields/@types";

const DEFAULT_EXPOSURE = 60;
const EXPOSURE_TIMES = [2, 4, 8, 15, 30, 60, 125, 250, 500, 1000, 2000];

export enum ExposureKind {
  Long = "long",
  Standard = "standard",
  Custom = "custom",
}

export enum LongExposureUnit {
  Seconds = "sec",
  Minutes = "min",
  Hours = "hrs",
}

export type ExposureTime =
  | {
      value: number;
      kind: ExposureKind.Custom | ExposureKind.Standard;
    }
  | {
      value: number;
      kind: ExposureKind.Long;
      unit: LongExposureUnit;
    };

export function useExposureTime() {
  const { formatExposureTimeHTML } = useDisplayFormatters();

  const standardExposureTimeOptions = EXPOSURE_TIMES.map((time, index) => {
    return {
      id: index,
      value: time,
      label: formatExposureTimeHTML(time),
    };
  });

  const longExposureTimeOptions = Object.values(LongExposureUnit).map(
    (unit, index) => {
      const option: Option = {
        id: index,
        value: unit,
      };

      switch (unit) {
        case LongExposureUnit.Seconds:
          option.label = "Seconds";
          break;
        case LongExposureUnit.Minutes:
          option.label = "Minutes";
          break;
        case LongExposureUnit.Hours:
          option.label = "Hours";
          break;
      }

      return option;
    }
  );

  return {
    EXPOSURE_TIMES,
    DEFAULT_EXPOSURE,
    standardExposureTimeOptions,
    longExposureTimeOptions,
  };
}
