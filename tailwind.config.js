const defaultTheme = require("tailwindcss/defaultTheme");
const Color = require("color");

const FUJI = [
  "hsl(157, 46%, 89%)",
  "hsl(157, 48%, 81%)",
  "hsl(157, 49%, 68%)",
  "hsl(157, 49%, 55%)",
  "hsl(157, 70%, 41%)",
  "hsl(157, 70%, 34%)",
  "hsl(157, 69%, 23%)",
  "hsl(159, 72%, 14%)",
  "hsl(159, 70%, 9%)",
];

const PORTRA = [
  "hsl(240, 30%, 96%)",
  "hsl(237, 31%, 89%)",
  "hsl(238, 30%, 81%)",
  "hsl(239, 30%, 74%)",
  "hsl(239, 30%, 66%)",
  "hsl(239, 16%, 51%)",
  "hsl(240, 15%, 36%)",
  "hsl(240, 15%, 21%)",
  "hsl(240, 16%, 6%)",
];

const KODAK = [
  "hsl(43.2, 100%, 95.1%)",
  "hsl(42.6, 100%, 85.1%)",
  "hsl(42.9, 100%, 72.5%)",
  "hsl(43, 100%, 62.5%)",
  "hsl(43, 100%, 50%)",
  "hsl(43, 100%, 37.5%)",
  "hsl(43.3, 100%, 27.5%)",
  "hsl(42.9, 100%, 15.1%)",
  "hsl(43.2, 100%, 4.9%)",
];

const CINESTILL = [
  "hsl(2.7, 78.6%, 94.5%)",
  "hsl(0.9, 76.2%, 83.5%)",
  "hsl(1, 75.3%, 69.8%)",
  "hsl(1.1, 75.2%, 58.8%)",
  "hsl(1, 92%, 45%)",
  "hsl(1.1, 91.9%, 33.7%)",
  "hsl(1, 92.1%, 24.7%)",
  "hsl(1, 91.3%, 13.5%)",
  "hsl(0, 91.3%, 4.5%)",
];

const GRAY = [
  "hsl(240, 20%, 98%)",
  "hsl(240, 3%, 87%)",
  "hsl(240, 2%, 75%)",
  "hsl(240, 2%, 64%)",
  "hsl(240, 2%, 52%)",
  "hsl(240, 2%, 41%)",
  "hsl(240, 3%, 29%)",
  "hsl(240, 5%, 18%)",
  "hsl(240, 16%, 6%)",
];

function makeTWColors(colors) {
  return colors.reduce((twColors, rawColor, index) => {
    const color = new Color(rawColor);

    const twKey = Number((index + 1) * 100).toString();
    twColors[twKey] = color.string();

    if (index === 4) twColors.DEFAULT = color.string();

    return twColors;
  }, {});
}

module.exports = {
  content: [
    "./resources/**/*.{vue,js,ts,jsx,tsx}",
    "./resources/app/app.blade.php",
  ],
  theme: {
    fontFamily: {
      sans: ["Mulish", ...defaultTheme.fontFamily.sans],
    },
    fontSize: {
      ...defaultTheme.fontSize,
      "big-logo": [
        "5rem",
        {
          letterSpacing: "-3.2px",
        },
      ],
    },
    colors: {
      transparent: "transparent",
      current: "currentColor",

      pureWhite: "#FFFFFF",
      white: {
        DEFAULT: GRAY[0],
        hover: new Color("hsl(0, 0%, 100%)").darken(0.075).string(),
      },
      black: GRAY[GRAY.length - 1],
      gray: makeTWColors(GRAY),

      fuji: makeTWColors(FUJI),
      portra: makeTWColors(PORTRA),
      kodak: makeTWColors(KODAK),
      cinestill: makeTWColors(CINESTILL),
    },

    extend: {
      screens: {
        xxs: "420px",
        xs: "576px",
      },
    },
  },
  plugins: [require("@tailwindcss/forms")],
};
