import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import laravel from "laravel-vite-plugin";

import VueTypeImports from "vite-plugin-vue-type-imports";

export default defineConfig({
  resolve: {
    alias: {
      "@": "/resources/",
    },
  },
  server: {
    host: true,
  },
  plugins: [
    laravel({ input: ["resources/scripts/main.ts"] }),
    vue({
      template: {
        compilerOptions: {
          isCustomElement: (tag) => tag.startsWith("ion-"),
        },
        transformAssetUrls: {
          // The Vue plugin will re-write asset URLs, when referenced
          // in Single File Components, to point to the Laravel web
          // server. Setting this to `null` allows the Laravel plugin
          // to instead re-write asset URLs to point to the Vite
          // server instead.
          base: null,

          // The Vue plugin will parse absolute URLs and treat them
          // as absolute paths to files on disk. Setting this to
          // `false` will leave absolute URLs un-touched so they can
          // reference assets in the public directly as expected.
          includeAbsolute: false,
        },
      },
    }),
    VueTypeImports(),
  ],
});
